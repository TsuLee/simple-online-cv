<?php
include_once 'includes/functions.php';
?>
<!doctype html>
<html lang="fr">
<head>
    <title>Cv Online | Khalid Tsouli</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/costum.css">
</head>
<body>
<div id="back-to-top"></div>
<div class="container bg-white">
    <!-- Header -->
    <header class="header row my-bg-dark text-white">
        <div class="col-12 col-md-4 px-0 bg-white text-center pb-0 pb-md-1">
            <img src="img/ma-photo.jpg" class="w-75" alt="">
        </div>
        <div class="col-12 col-md-8 mb-md-5">
            <div class="row px-3 py-3 px-md-4 py-md-5">
                <div class="title-header col-12">
                    <h4 class="mb-0 text-uppercase">PRÉSENTATION</h4>
                </div>
                <div class="col-12 mt-4 mb-0 mb-md-5 pr-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci amet blanditiis ea enim est id laboriosam modi molestias. N
                        eque perferendis recusandae repellendus repudiandae tenetur ullam! Accusamus aliquid animi.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, nostrum.
                    </p>
                </div>
            </div>
        </div>
        <div class="row w-100 mx-0 banner-header">
            <div class="col-12 my-bg-medium py-3 px-4 w-100">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <h4>John Doe</h4>
                        <h5 class=""><em>Développeur Web et App</em></h5>
                    </div>
                    <div class="col-12 col-md-8 d-flex align-items-center justify-content-end mt-3 mt-md-0">
                        <a type="button" class="small-text btn btn-secondary ml-2" href="#" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> En PDF</a>
                        <a type="button" class="small-text btn btn-secondary ml-2" href="https://gitlab.com/TsuLee/simple-online-cv" target="_blank"><i class="fa fa-gitlab" aria-hidden="true"></i> Code Source</a>
                    </div>
                </div>

            </div>
        </div>
    </header>

    <!-- Content -->
    <section id="sidebar" class="row">

        <!-- Sidebar -->
        <div class="col-12 col-md-4 my-bg-gray px-4 py-5">

            <!-- CONTACT -->
            <div class="row">
                <div class="title col-12">
                    <h5>CONTACT</h5>
                </div>
                <div class="col-12 py-3 pr-0">
                    <ul class="list-unstyled">
                        <li><i class="fa fa-home fa-1x mr-2 mb-3" aria-hidden="true"></i> 50 Mon adresse, ville, 66000.</li>
                        <li><i class="fa fa-phone-square fa-1x mr-2 mb-3" aria-hidden="true"></i> 01 01 01 01 01</li>
                        <li><i class="fa fa-envelope fa-1x mr-2 mb-3" aria-hidden="true"></i>  <a HREF="mailto:khalid.tsouli@gmail.com.">JohnDoe@Cv.com.</a></li>
                        <li><i class="fa fa-globe fa-1x mr-2 mb-3" aria-hidden="true"></i> <a href="#" target="_blank">JohnDoe.com/JohnDoe <i class="fa fa-eye" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>

            <!-- ÉDUCATION -->
            <div class="row education">
                <div class="title col-12">
                    <h5>ÉDUCATION</h5>
                </div>
                <div class="col-12 py-3 pr-0">
                    <ul class="list-unstyled">
                        <li><i class="fa fa-graduation-cap fa-1x mr-2 mb-2" aria-hidden="true"></i> <strong>Ma formation 3.</strong><br>
                            <span class="ml-4">2020-2021</span>
                            <p class="mb-4 ml-3 pl-2 mt-2">Lorem ipsum dolor sit amet, consectetur.</p>
                        </li>

                        <li><i class="fa fa-graduation-cap fa-1x mr-2 mb-2" aria-hidden="true"></i> <strong>Ma formation 2.</strong><br>
                            <span class="">2013-2015</span>
                            <p class="mb-4 ml-3 pl-2 mt-2">Lorem ipsum dolor sit amet, consectetur.</p>
                        </li>

                        <li><i class="fa fa-graduation-cap fa-1x mr-2 mb-2" aria-hidden="true"></i> <strong>Ma formation 1.</strong><br>
                            <span class="">1998</span>
                            <p class="mb-4 ml-3 pl-2 mt-2">Lorem ipsum dolor sit amet, consectetur.</p>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- LANGUES  -->
            <div class="row">
                <div class="title col-12">
                    <h5>LANGUES</h5>
                </div>
                <div class="col-12 py-3 pr-0">
                    <ul class="list-unstyled">
                        <li class="site mb-4"><i class="fa fa-language fa-1x mr-2 mb-2" aria-hidden="true"></i> <strong>Langue 1</strong><br>
                            <span class="ml-4">Langue maternelle.</span>
                        </li>

                        <li class="site mb-4"><i class="fa fa-language fa-1x mr-2 mb-2" aria-hidden="true"></i> <strong>Langue 2</strong><br>
                            <span class="ml-4">Seconde langue maternelle.</span>
                        </li>

                        <li class="site mb-4"><i class="fa fa-language fa-1x mr-2 mb-2" aria-hidden="true"></i> <strong>Langue 3</strong><br>
                            <span class="ml-4">Bilingue.</span>
                        </li>

                        <li class="site mb-4"><i class="fa fa-language fa-1x mr-2 mb-2" aria-hidden="true"></i> <strong>Langue 4</strong><br>
                            <span class="ml-4">Conversation professionnelle.</span>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- À savoir -->
            <div class="row">
                <div class="title col-12">
                    <h5>À savoir</h5>
                </div>
                <div class="col-12 py-3 pr-0">
                    <div class="text-white">
                        <div class="bg-secondary float-left px-3 py-1">Permis</div>
                        <div class="my-bg-medium float-left px-3 py-1">B</div>
                    </div>
                </div>

                <div class="col-12 py-3 pr-0">
                    <div class="text-white">
                        <div class="bg-secondary float-left px-3 py-1">Vehiculé</div>
                        <div class="my-bg-medium float-left px-3 py-1">Oui</div>
                    </div>
                </div>

                <div class="col-12 py-3 pr-0">
                    <div class="text-white">
                        <div class="bg-secondary float-left px-3 py-1">Mobilité</div>
                        <div class="my-bg-medium float-left px-3 py-1">20-30km</div>
                    </div>
                </div>

                <div class="col-12 py-3 pr-0">
                    <div class="text-white">
                        <div class="bg-secondary float-left px-3 py-1">Disponibilité</div>
                        <div class="my-bg-medium float-left px-3 py-1">09/12/2020</div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Center -->
        <div class="col-12 col-md-8">



            <!-- RÉALISATIONS -->
            <div class="row mb-4">
                <div class="title-center col-12 px-4 py-5 mb-4">
                    <h5>RÉALISATIONS<span class="text-muted ml-1" style="font-size: 9px;"> (exemples)</span></h5>

                </div>
                <?php get_works(
                    'MyProject.fr',
                    'HTML, CSS, PHP, JavaScript, jQuery, MYSQL, AJAX, Bootstrap',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi corporis culpa, deserunt enim eos laborum possimus reiciendis sequi voluptas voluptates.');

                get_works(
                    'MyProject.fr',
                    'HTML, CSS, PHP, JavaScript, jQuery, MYSQL, AJAX, Bootstrap',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi corporis culpa, deserunt enim eos laborum possimus reiciendis sequi voluptas voluptates.');

                get_works(
                    'MyProject.fr',
                    'HTML, CSS, PHP, JavaScript, jQuery, MYSQL, AJAX, Bootstrap',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi corporis culpa, deserunt enim eos laborum possimus reiciendis sequi voluptas voluptates.');

                get_works(
                    'MyProject.fr',
                    'HTML, CSS, PHP, JavaScript, jQuery, MYSQL, AJAX, Bootstrap',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi corporis culpa, deserunt enim eos laborum possimus reiciendis sequi voluptas voluptates.');

                get_works(
                    'MyProject.fr',
                    'HTML, CSS, PHP, JavaScript, jQuery, MYSQL, AJAX, Bootstrap',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi corporis culpa, deserunt enim eos laborum possimus reiciendis sequi voluptas voluptates.');

                get_works(
                    'MyProject.fr',
                    'HTML, CSS, PHP, JavaScript, jQuery, MYSQL, AJAX, Bootstrap',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi corporis culpa, deserunt enim eos laborum possimus reiciendis sequi voluptas voluptates.');
                ?>
            </div>

            <!-- COMPÉTENCES -->
            <div id="competences" class="row">
                <div class="title-center col-12 px-4 py-5 mb-4">
                    <h5>COMPÉTENCES</h5>
                </div>
                <div class="row px-0 px-md-3 mx-0">

<!-- NULL for progress bar. Set value and activate design on functions -->
                        <?php get_competences('html.png', 'HTML', NULL);?>
                        <?php get_competences('css.png', 'CSS', '90');?>
                        <?php get_competences('javascript.png', 'JavaScript', NULL);?>
                        <?php get_competences('jquery.png', 'jQuery', NULL);?>
                        <?php get_competences('angularjs.png', 'Angular', NULL);?>
                        <?php get_competences('php.png', 'PHP', NULL);?>
                        <?php get_competences('mysql.png', 'Mysql', NULL);?>
                        <?php get_competences('python.png', 'Python', NULL);?>
                        <?php get_competences('symfony.png', 'Symfony', NULL);?>
                        <?php get_competences('wordpress.png', 'Wordpress', NULL);?>
                        <?php get_competences('bootstrap.png', 'Bootstrap', NULL);?>
                        <?php get_competences('Adobe.png', 'Adobe (Photoshop, Premier, Dreamweaver)', NULL);?>
                        <?php get_competences('git.png', 'Git', NULL);?>
                        <?php get_competences('seo.png', 'SEO/SEM', NULL);?>

                </div>
            </div>

            <!-- EXPÉRIENCES -->
            <div class="row">
                <div class="title-center col-12 px-4 py-5 mb-4">
                    <h5>EXPÉRIENCES</h5>
                </div>
                <?php get_experience(
                    'Expérience 1',
                    '08/2020',
                    'TheWeb.com',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam delectus tempora?',
                    true);

                get_experience(
                    'Expérience 2',
                    '08/2020',
                    'TheWeb.com',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam delectus tempora?',
                    true);

                get_experience(
                    'Expérience 3',
                    '08/2020',
                    'TheWeb.com',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam delectus tempora?',
                    false);
                get_experience(
                    'Expérience 4',
                    '08/2020',
                    'TheWeb.com',
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam delectus tempora?',
                    true);
                ?>
            </div>

        </div>
    </section>


    <footer class="row my-bg-dark p-4 text-white text-center">
<p class="col-12">Cv Online réalisé avec Html, Css, Js, Php et Bootstrap. (BDD innécessaire).</p>
        <p class="col-12">Code source de cette page bientôt disponible sur <a href="https://gitlab.com/TsuLee/simple-online-cv" target="_blank" <i class="fa fa-gitlab" aria-hidden="true"></i> GitLab</a></p>
    </footer>
    <div id="go-top">
        <a href="#back-to-top"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
    </div>



</div> <!-- End Container -->


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/39b7f10b93.js"></script>
<script src="js/costum.js"></script>
</body>
</html>