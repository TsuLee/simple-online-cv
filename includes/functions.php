<?php
function get_competences( $img, $img_alt, $percent ){
    echo '
        
            <div class="col-3 col-md-2 text-center mb-3">
                <img src="img/' . $img . '" class="w-75 mb-2" alt="'. $img_alt .'" title="'. $img_alt .'">
                <!-- set $percent and activate this html if you want a bar
                
                 <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-secondary" role="progressbar" style="width: '. $percent .'%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                    </div>  
              </div>
              -->
            </div>
        ';
}

function get_experience($fonction, $date, $work, $description, $link){

echo '
        <div class="row w-100 px-3 mx-0 mb-3 border-bottom">
            <div class="col-12 col-md-5 px-3">
                <div>
                    <h6 class="font-weight-bolder">' . $fonction . '</h6>
                    <span class="">' . $date . '</span>
                </div>
            </div>
            <div class="col-12 col-md-7 px-3 mt-3 mt-md-0">';

if ($link){
    /* Links disabled on demo */
    echo '<h6 class="font-weight-bold"><a href="#" target="_blank">' . $work . ' <i class="fa fa-eye" aria-hidden="true"></i></a></h6>';
    /* To enable activate this one*/
//    echo '<h6 class="font-weight-bold"><a href="http://www.' . $work . '" target="_blank">' . $work . ' <i class="fa fa-eye" aria-hidden="true"></i></a></h6>';
} else {
    echo '<h6 class="font-weight-bold">' . $work . '</h6>';
}
    echo'
                <p>' . $description . '</p>
            </div>
        </div>
        
';
}

function get_works($work, $program, $description){
    echo '
        <div class="row w-100 px-3 mx-0 mb-3 border-bottom">
            <div class="col-12 col-md-5 px-3">
                <div>
                <!--Links disabled on demo -->
                    <h6 class="font-weight-bolder"><a href="#" target="_blank">  ' . $work . ' <i class="fa fa-eye" aria-hidden="true"></i></a></h6>
                    <!--
                    To enable activate this one
                    <h6 class="font-weight-bolder"><a href="http://www.' . $work . '" target="_blank">  ' . $work . ' <i class="fa fa-eye" aria-hidden="true"></i></a></h6>
                    -->
                    <span class="">' . $program . '</span>
                </div>
            </div>
            <div class="col-12 col-md-7 px-3">
                <p>' . $description . '</p>
            </div>
        </div>
';
}