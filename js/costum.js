jQuery(document).ready(function($) {
    function univers(e) {
        $(e).click(function() {
            $('html,body').animate({scrollTop: 0}, 'slow');
        });

        $(window).scroll(function(){
            if($(window).scrollTop()<800){
                $(e).fadeOut();
            } else{
                $(e).fadeIn();
            }
        });
    }
    univers("#go-top");
});
